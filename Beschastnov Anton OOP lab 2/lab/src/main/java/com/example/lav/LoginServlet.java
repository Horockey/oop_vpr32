package com.example.lav;

import java.io.*;
import java.util.Map;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "login", value = "/login")
public class LoginServlet extends HttpServlet {
    private String message_error;

    private String error_login;

    private String success_login;

    private String youDay;

    private boolean isAuthorication;

    private int scope;

    private static final int DEFAULT = 0;

    private Pair[] user_data;


    public void init() {
        user_data = new Pair[]{
                new Pair<String, String>("pupol@mail.ru", "1234"),
                new Pair<String, String>("superman1234@gmail.com", "4321"),
                new Pair<String, String>("anatolii2002@mail.ru", "4444"),
                new Pair<String, String>("stepan@mail.ru", "4242"),
                new Pair<String, String>("stepan2@mail.ru", "2424"),
                new Pair<String, String>("opaopaopa@mail.ru", "3333")
        };
        message_error = "Ага! не все параметры были введены!!!";
        error_login = "Неправильно введен логин или пароль!";
        success_login = "Вы успешно авторизировались!";
        isAuthorication = false;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");

        Map<String, String[]> params = request.getParameterMap();

        for ( Map.Entry<String, String[]> param :params.entrySet()) {
            if (param.getValue()[0] == null) {
                out.println("<h1>" + message_error + "</h1>");
                out.println("</body></html>");
                return;
            }
        }

        for (Pair data : user_data) {
            if (
                    (
                    data.getPartOne().equals(params.get("email")[DEFAULT])
                    &&
                    data.getPartTwo().equals(params.get("passoword")[DEFAULT])
                    )
            ) {
                isAuthorication = true;
                break;
            }
        }

        if (!isAuthorication) {
            out.println("<h1>" + error_login + "</h1>");
            out.println("</body></html>");
            return;
        }
        for (String num : params.get("mood")) {
           scope += Integer.parseInt(num);
        }


        if (scope == 5) {
            youDay = "Все довольно хорошо!Еще чуть чуть и будет отлично!";
        } else if (scope == 4) {
            youDay = "Все довольно хорошо!Еще чуть чуть и будет отлично!";
        } else if (scope == 1) {
            youDay = "Ну!Все не так плохо!";
        } else if (scope >= 5) {
            youDay = "А ты хитрец! Думал так нельзя?) Мы рады что твой день лучше чем обычно";
        } else if (scope > 2) {
            youDay = "Главное что все не ужастно!";
        } else {
            youDay = "Все будет хорошо!!";
        }

        out.println("<h1>" + success_login + "</h1>");
        out.println("<h4>" + "Ваш email: " + params.get("email")[DEFAULT] + "</h1>");
        out.println("<h4>" + "Ваша должность: " + params.get("positions")[DEFAULT] + "</h1>");
        out.println("<h4>" + "По вашей оценке на сегодняшний день мы можем сказать:  " + youDay + "</h1>");
        out.println("<h4>" + "И мы тоже желаем вам: " + params.get("comment")[DEFAULT] + "</h1>");
        out.println("<img src=\"https://fikiwiki.com/uploads/posts/2022-02/1644927822_23-fikiwiki-com-p-smeshnie-kartinki-pro-kotyat-23.jpg\">");
        out.println("</body></html>");

        isAuthorication = false;
    }

    public void destroy() {
    }
}