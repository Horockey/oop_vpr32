package com.example.lav;

public class Pair<T , V> {
    private T partOne;
    private V partTwo;

    public Pair(T partOne, V partTwo) {
        this.partOne = partOne;
        this.partTwo = partTwo;
    }

    public Pair() {
        this.partOne = null;
        this.partTwo = null;
    }

    public T getPartOne() {
        return partOne;
    }

    public V getPartTwo() {
        return partTwo;
    }

    public void setPartOne(T partOne) {
        this.partOne = partOne;
    }

    public void setPartTwo() {
        this.partTwo = partTwo;
    }
}
