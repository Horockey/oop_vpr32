<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>My Secret Form</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
    <body>
        <h1 style="text-align: center"><%= "My Secret Form:" %></h1>
            <form method="get" action="login">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Ваш email: </label>
                    <input name="email" type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль: </label>
                    <input name="passoword" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Выберите свою должность: </label>
                    <select name="positions" class="form-control" id="exampleFormControlSelect1" required>
                        <option>Офицер</option>
                        <option>Клерк</option>
                        <option>Бариста</option>
                        <option>Охраник</option>
                        <option>Менеджер</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Выбирите оценку сегодняшнего пароль: </label>
                    <select  name="mood" multiple class="form-control" id="exampleFormControlSelect2" required>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Что вы пожелаете своим коллегам: </label>
                    <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                </div>
                <button style="margin-left: 92%" type="submit" class="btn btn-primary">Отправить</button>
            </form>
        <br/>
    </body>
</html>