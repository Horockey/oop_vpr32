# Форма авторизации

Рассмотрим создание формы авторизации с использованием технологии Jakarta EE  и сервера Tomcat в IntelliJ IDEA IDE.

Для осуществления нижеописанных действий на компьютере должны быть установлены:

* Tomcat 9
* JavaX/Jakarta EE

## Шаг 1

Создаём в Intellij Idea проект Jakarta E E указывая в Template Web application.

<img style="float: center;" src="docs/img_1_1.png">
<p style="text-align: center;">Рисунок 1.1 - Создание проекта</p>

В Project Structure подключаем библиотеку Java EE

<img style="float: center;" src="docs/img_1_2.png">
<p style="text-align: center;">Рисунок 1.2 - Подключение Java EE</p>

В настройках конфигурации настраиваем Tomcat

<img style="float: center;" src="docs/img_1_3.png">
<p style="text-align: center;">Рисунок 1.3 - Настройка Tomcat</p>

Настройка завершена, приступаем к написанию решения.

## Шаг 2

Описываем `first.html`, помещаем поля ввода логина и пароля, кнопку отправки

<img style="float: center;" src="docs/img_2_1.png">
<p style="text-align: center;">Рисунок 2.1 - HTML страницы авторизации</p>

Описываем `first.xml`, связываем endpoint `/first` с именем сервлета `FirstServ`, который мы напишем далее

<img style="float: center;" src="docs/img_2_2.png">
<p style="text-align: center;">Рисунок 2.2 - XML страницы авторизации</p>

## Шаг 3

Описываем сервлет, отвечающий зав авторизацию пользователя

```java

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

@WebServlet("/first")
public class FirstServ extends HttpServlet {

    private ArrayList<String> IDofSession = new ArrayList<>();
    private ArrayList<Integer> countOfSession = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession s = req.getSession();
        System.out.println("++++++++++++++" + s);
        String tmp = s.getId();
        int index = -1;
        boolean flag = false;
        for (int i = 0; i < IDofSession.size(); i++){
            if (IDofSession.get(i).equals(tmp)){
                index = i;
                flag = true;
                Integer count = countOfSession.get(i);
                count++;
                countOfSession.set(i, count);
                if (count >= 3 && req.getSession().getId().equals(IDofSession.get(i))){
                    resp.setContentType("text/html");
                    PrintWriter printWriter = resp.getWriter();
                    printWriter.write("Access denied");
                    printWriter.close();
                    return;
                }
                break;
            }
        }

        if (!flag){
            IDofSession.add(s.getId());
            countOfSession.add(0);
            index = IDofSession.size() - 1;
        }

        String log = req.getParameter("login");
        System.out.println("++++++++++++++++++++++++" + log);
        String pasw = req.getParameter("pass");
        System.out.println("++++++++++++++++++++++++" + pasw);

        File fl = new File("C:\\Tomcat\\tomcat-9.0.54\\apache-tomcat-9.0.54\\webapps\\lab2\\infoAboutPol.txt");
        FileReader fr = new FileReader(fl);
        Scanner sfr = new Scanner(fr);
        String usLog = sfr.next();
        String usPas = sfr.next();

        String textAnswer = "";
        if (log != null && !log.isEmpty() && pasw != null && !pasw.isEmpty()){
            textAnswer = "<p style=\"color:#ff0000\">";
            if ( log.equals(usLog) && pasw.equals(usPas)){
                textAnswer += "Welcome," + log + " Time: ";
                Date date = new Date();
                textAnswer += date.toString();

                countOfSession.set(index, 0);
            } else {
                int c = countOfSession.get(index);
                c++;
                countOfSession.set(index, c);
                textAnswer += "try Again, count: " + Integer.toString(c);
            }
            textAnswer += "</p>";

        }

        resp.setContentType("text/html");
        PrintWriter printWriter = resp.getWriter();
        File f = new File("C:\\Tomcat\\tomcat-9.0.54\\apache-tomcat-9.0.54\\webapps\\lab2\\WEB-INF\\classes\\info.txt");
        String text = "";
        try (Scanner scan = new Scanner(f).useDelimiter("\\n")) {
            while(scan.hasNext()) {
                text += scan.next();
            }
        }
        printWriter.write(text + textAnswer);
        printWriter.close();
    }
}


```

Логика обработки запроса следующая:

1. У пользователя 3 попытки на ввод парвильных логин-пароля
2. Правильные логин-пароли хранятся в отдельном файле. Обратите внимание на его расположение - **в директории Tomcat**, но об этом чуть позже
3. При удачном вводе попроветствуем пользователя его логином и временем входа
4. При неудачной попытке входа выведем об этом сообщение и выведем количество попыток

## Шаг 4

Собираем проект, добавляем артефакт сборки в директиву Tomcat. По мере роста этот этап (deploy) стоит автоматизировать

<img style="float: center;" src="docs/img_4_1.png">
<p style="text-align: center;">Рисунок 4.1- Артефакт сборки</p>

## Шаг 5

Запускаем проект. Вы восхитительны!

<img style="float: center;" src="docs/img_5_1.png">
<p style="text-align: center;">Рисунок 5.1 - Внешний вид</p>